
document.querySelector(".tabs").addEventListener("click", (event) => {
    document.querySelectorAll(".tabs-title").forEach((elem) => {
        elem.classList.remove("active");
    });
    event.target.classList.add("active");
    let activeContent = event.target.getAttribute("data-content");
    document.querySelectorAll(".tab-content").forEach((elem) => {
        if (elem.getAttribute("data-content") === activeContent) {
            elem.style.display = "block";
        } else {
            elem.style.display = "none";
        }
    })
});
let currentActive = document.querySelector(".active");
document.querySelectorAll(".tab-content").forEach((elem) => {
    if (currentActive.getAttribute("data-content") === elem.getAttribute("data-content")) {
        elem.style.display = "block";
    } else {
        elem.style.display = "none";
    }
});